from django.shortcuts import render
from django.contrib.auth.models import User, Group
from .models import more_about_user,catalog_first_lvl,catalog_sec_lvl,comments,reviews,good
from rest_framework import viewsets
from rest_framework import permissions
from main_functional.serializers import UserSerializer, GroupSerializer, More_about_userSerializer, ReviewsSerialiser, CommentsSerialiser, GoodSerialiser, Catalog_sec_lvl_serialiser, CatalogSerialiser   



class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]

class CatalogViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = catalog_first_lvl.objects.all().order_by('id')
    serializer_class = CatalogSerialiser
    # permission_classes = [permissions.IsAuthenticated]

class CatalogSecViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = catalog_sec_lvl.objects.all().order_by('id')
    serializer_class = Catalog_sec_lvl_serialiser

class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]