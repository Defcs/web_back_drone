from django.contrib.auth.models import User, Group
from .models import more_about_user,catalog_first_lvl,catalog_sec_lvl,comments,reviews,good
from rest_framework import serializers


class More_about_userSerializer(serializers.ModelSerializer):
    class Meta:
        model = more_about_user
        fields = ['date_birth', 'gender', 'real_name','real_sename', 'id','token']


class UserSerializer(serializers.HyperlinkedModelSerializer):
    more_about_users = More_about_userSerializer(many = False,read_only=True)
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups','more_about_users']

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class ReviewsSerialiser(serializers.HyperlinkedModelSerializer):
    user = UserSerializer(many = False,read_only=True)
    class Meta:
        model = reviews
        fields = ['id', 'title', 'desc','stars','user']

class CommentsSerialiser(serializers.HyperlinkedModelSerializer):
    user = UserSerializer(many = False,read_only=True)
    class Meta:
        model = comments
        fields = ['id', 'title', 'desc','stars','user']


class GoodSerialiser(serializers.HyperlinkedModelSerializer):
    comments = CommentsSerialiser(many = False,read_only=True)
    reviews = ReviewsSerialiser(many = False,read_only=True)
    class Meta:
        model = good
        fields = ['id', 'title', 'desc','charact','count','customer','img','link','stars','new_price','old_price','material','speed','fly_time','comments','reviews']

        # def get_customer(self,obj):
        #     return [customer.username for customer in obj.customer.all()]

class Catalog_sec_lvl_serialiser(serializers.ModelSerializer):
    goods_cat = GoodSerialiser(many = True,read_only=True)
    class Meta:
        model = catalog_sec_lvl
        fields = ['id', 'title', 'img','link','goods_cat']

class CatalogSerialiser(serializers.ModelSerializer):
    catalog_sec = Catalog_sec_lvl_serialiser(many=True,read_only=True)
    class Meta:
        model = catalog_first_lvl
        fields = ['id', 'title', 'img','link','catalog_sec']



