# Generated by Django 4.2.1 on 2023-06-09 19:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main_functional', '0003_alter_more_about_user_date_birth'),
    ]

    operations = [
        migrations.AddField(
            model_name='more_about_user',
            name='token',
            field=models.CharField(blank=True, default='Неизвестно', max_length=12, null=True, verbose_name='Фамилия'),
        ),
    ]
