from django.urls import include, path
from django.contrib.auth import update_session_auth_hash
from .models import more_about_user,catalog_first_lvl,catalog_sec_lvl,comments,reviews,good
from .resources import auth_resource
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets


app_name = 'main_functional'




urlpatterns = [

    path('registration', auth_resource.registrate),
    path('delete_user', auth_resource.delete_user),
    path('authorisation', auth_resource.authorisation),
    path('update_user', auth_resource.update_user),
    # path('about us/', views.aboutUs, name='aboutUs'),
    # # path('registration', Auth_Controller.registration, name='registration'),
    
    # path('logout', Auth_Controller.logout_view, name='logout'),
    # path('registration', Auth_Controller.registration, name='registration'),
    # path('registrationpost', Auth_Controller.registrationpost, name='registrationpost'),
    # path('authorisation', Auth_Controller.authorisation, name='authorisation'),
    # path('authorisationpost', Auth_Controller.authorisationpost, name='authorisationpost'),
    # path('userdel', Auth_Controller.userdel, name='userdel'),

    # path('schedule', Schedule_Controller.schedule, name='schedule'),
    # path('schedule/del/<int:pk>', Schedule_Controller.schedule_del, name='schedule_del'),
    # path('schedule/<int:pk>', Schedule_Controller.schedule_more, name='schedule_more'),
    # path('schedule/add_comment/<int:pk>', Schedule_Controller.add_comment, name='add_comment'),
    # path('schedule/del_com/<int:pk>', Schedule_Controller.del_com, name='del_com'),
    # path('schedule/up_com/<int:pk>', Schedule_Controller.up_com, name='up_com'),
    # path('schedule/up_com/update_comment/<int:pk>', Schedule_Controller.update_com, name='update_com'),

    # path('history', History_Controller.get_information_about_model, name='history'),

    # path('export_Db_trainings', Export_Db_Controller.trainings_import, name='trainings'),
    
    
    # path('account/', views.account, name='account'),
    
    
    # path('', include('django.contrib.auth.urls')),
    # path('login/', views.login, name='login'),
    # path('location/', views.location, name='location'),
    
    # path('schedule/', views.schedul, name='schedul'),
    # path('account/my schedule/<int:users>', views.Myschedule, name='Myschedule'),
]