from django.apps import AppConfig


class MainFunctionalConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'main_functional'
