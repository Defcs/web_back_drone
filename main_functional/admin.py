from django.contrib import admin
from .models import more_about_user,catalog_first_lvl,catalog_sec_lvl,comments,reviews,good
# from import_export import resources

# Register your models here.

class More_about_user(admin.ModelAdmin):
    list_display = ('id', 'user', 'date_birth','gender')
    list_filter = ['date_birth']
    list_filter = ['gender']

class Catalog_first_lvl(admin.ModelAdmin):
    list_display = ('id', 'title', 'img','link')
    list_filter = ['id']

class Catalog_sec_lvl(admin.ModelAdmin):
    list_display = ('id', 'title', 'img','link','catalog_first')
    list_filter = ['id']

class Comments(admin.ModelAdmin):
    list_display = ('id', 'title', 'desc','stars','user','tw')
    list_filter = ['id']

class Comments(admin.ModelAdmin):
    list_display = ('id', 'title', 'desc','stars','user','tw')
    list_filter = ['id']

class Reviews(admin.ModelAdmin):
    list_display = ('id', 'title', 'desc','stars','user','tw')
    list_filter = ['id']

class Good(admin.ModelAdmin):
    list_display = ('id', 'title', 'desc','charact','count','get_customer','img','link','stars','new_price','old_price','material','speed','fly_time','sec_lvl')
    list_filter = ['id']

    def get_customer(self,obj):
        return [customer.username for customer in obj.customer.all()]

admin.site.register(more_about_user, More_about_user)
admin.site.register(catalog_first_lvl, Catalog_first_lvl)
admin.site.register(catalog_sec_lvl, Catalog_sec_lvl)
admin.site.register(comments, Comments)
admin.site.register(reviews, Reviews)
admin.site.register(good, Good)